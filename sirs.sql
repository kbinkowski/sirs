﻿-- TABLES
DROP TABLE IF EXISTS account CASCADE;
DROP TABLE IF EXISTS file_content CASCADE ;
DROP TABLE IF EXISTS file_info CASCADE;

CREATE TABLE account
(
  login character varying(25) NOT NULL,
  password character varying(256),
  size INTEGER,
  CONSTRAINT account_pkey PRIMARY KEY (login)
);

CREATE TABLE file_info (
	login VARCHAR(25),
	path VARCHAR(255),
	datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP(0),
	checksum VARCHAR(40),
	size INTEGER,
	PRIMARY KEY (login, path, datetime)
);

CREATE TABLE file_content
(
	login VARCHAR(25),
	path VARCHAR(255),
	datetime TIMESTAMP,
	file BYTEA,
	PRIMARY KEY (login, path, datetime),
	FOREIGN KEY (login, path, datetime) REFERENCES file_info (login, path, datetime) ON DELETE CASCADE
);

-- TRIGGERS

CREATE OR REPLACE FUNCTION add_file_func() RETURNS TRIGGER
AS
$$
DECLARE
	sizeClient INTEGER;
BEGIN
	SELECT size INTO sizeClient FROM account WHERE login = NEW.login;
	IF sizeClient + NEW.size > 52428800 THEN
		RAISE EXCEPTION 'Exceeded of limit space per user.';
	END IF;
	UPDATE account SET size = size + NEW.size WHERE login = NEW.login;
	RETURN NEW;
END
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_file_func() RETURNS TRIGGER
AS
$$
BEGIN
	UPDATE account SET size = size - OLD.size WHERE login = OLD.login;
	RETURN NEW;
END
$$ 
LANGUAGE plpgsql;

CREATE TRIGGER add_file BEFORE INSERT OR UPDATE ON file_info
FOR EACH ROW EXECUTE PROCEDURE add_file_func();

CREATE TRIGGER remove_file AFTER DELETE ON file_info
FOR EACH ROW EXECUTE PROCEDURE remove_file_func();

SELECT * FROM account;
