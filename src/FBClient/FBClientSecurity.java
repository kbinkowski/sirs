package FBClient;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.KeyPair;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import SecurityUtils.SecurityUtils;

public class FBClientSecurity {

	KeyPair keyPair;
	SecretKey sessionKey;
	SecretKey passwordKey;
	Integer challenge;
	SecretKey cipherKey;

	public byte[] authInitArguments(String pass) throws Exception {
		byte[] passHash = SecurityUtils.hashArray("SHA-256", pass.getBytes());
		keyPair = SecurityUtils.generateKeyPair("RSA", 512);
		passwordKey = SecurityUtils.convertHash2Key("AES", passHash);
		return SecurityUtils.cipherArray("AES", passwordKey,
				Cipher.ENCRYPT_MODE, keyPair.getPublic().getEncoded());

	}

	public void obtainSessionKey(byte[] message) throws Exception {
		byte[] decryptedMessage = SecurityUtils.cipherArray("AES", passwordKey,
				Cipher.DECRYPT_MODE, message);
		byte[] decryptedSessionKey = SecurityUtils.cipherArray("RSA",
				keyPair.getPrivate(), Cipher.DECRYPT_MODE, decryptedMessage);
		sessionKey = SecurityUtils.convertHash2Key("AES", decryptedSessionKey);
	}

	public byte[] generateChallenge() throws Exception {
		Random generator = new Random(System.currentTimeMillis());
		challenge = generator.nextInt();
		return SecurityUtils.cipherArray("AES", sessionKey,
				Cipher.ENCRYPT_MODE, challenge.toString().getBytes());

	}

	public byte[] respond2Challenge(byte[] encryptedChalange) throws Exception {
		byte[] decryptedMessage = SecurityUtils.cipherArray("AES", sessionKey,
				Cipher.DECRYPT_MODE, encryptedChalange);
		String[] numbers = (new String(decryptedMessage)).split("\\|");
		Integer clientChallenge = Integer.parseInt(numbers[0]);
		Integer serverChallenge = Integer.parseInt(numbers[1]);
		if (!clientChallenge.equals(challenge))
			throw new Exception();
		return SecurityUtils.cipherArray("AES", sessionKey,
				Cipher.ENCRYPT_MODE, serverChallenge.toString().getBytes());
	}

	public void initCipherKey(String pass) throws Exception {
		cipherKey = SecurityUtils.convertHash2Key("AES",
				SecurityUtils.hashArray("MD5", pass.getBytes()));
	}

	public byte[] cipherFile(InputStream is, int mode) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			SecurityUtils.cipherStream("AES", cipherKey, mode, is, os);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return os.toByteArray();
	}

	public byte[] cipherMessage(byte[] msg, int mode) {
		try {
			return SecurityUtils.cipherArray("AES", sessionKey, mode, msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
