package FBClient;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.crypto.Cipher;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;

import FBServer.FBServerInterface;
import FBServer.message.CheckFileMessage;
import FBServer.message.GetFilesMessage;
import FBServer.message.ReceiveFileMessage;
import FBServer.message.RemoveFilesMessage;
import FBServer.message.SendFileMessage;

import com.healthmarketscience.rmiio.RemoteInputStreamClient;
import com.healthmarketscience.rmiio.SimpleRemoteInputStream;

/**
 * Klasa reprezentujaca klienta uslugi archiwizacji plikow.
 * @author F-B team
 */
public class FBClient extends UnicastRemoteObject implements FBClientInterface{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Pole reprezentujace widok  klienta. 
	 */
	FBClientView clientView;
	
	/**
	 * Referencja na zdalny obiekt serwera aplikacji archiwizacji plikow.
	 */
	FBServerInterface server;
	
	/**
	 * Flaga informujaca czy klient jest zalogowany. 
	 */
	boolean logFlag;
	
	/**
	 * Nazwa klienta zalogowanego. 
	 */
	String name;	
	
	/**
	 * Kolekcja przechowujaca informacje o zarchiwizowanych plikach klienta. 
	 */
	HashMap<String, ArrayList<String> > remoteFiles;
	
	/**
	 * Kolekcja przechowujaca informacje o plikach klienta do zarchiwizowania. 
	 */
	HashSet<String> localFiles;
	
	private FBClientSecurity clientSecurity;
	
	/**
	 * Konstruktor klienta uslugi archiwizatora.
	 * @throws RemoteException 
	 */
	FBClient() throws RemoteException{
		super();
		clientSecurity = new FBClientSecurity();
		logFlag = false;
		remoteFiles = new HashMap<String, ArrayList<String> >();
		
        final FBClient me = this;
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try { clientView = new FBClientView(me); } 
				catch (Exception e) {
					System.err.println("Error creating ClientView: " + e);
					e.printStackTrace();
		            System.exit(1);
				}
	        }
	    });
	}
	
	/**
	 *Metoda informujaca czy klient jest zalogowany. Uzywana w widoku do aktualizacji stanu.
	 */
	boolean isLogOn(){
		return logFlag;
	}
	
	/**
	 * Metoda logowania uzytkownika do serwera. Zwraca czy zalogowanie przebieglo poprawnie.
	 */	
	boolean logOn(String server, String userName, String userPass){ 
        Remote remoteObject = null;
        try { remoteObject = Naming.lookup(server); } 
        catch (Exception e) {
            printInfo("Error. Server hasn't been recognized: " + e);
            return false;
        }

        if (remoteObject instanceof FBServerInterface)
            this.server = (FBServerInterface) remoteObject;
        else {
        	printInfo("Error. Server with a given name doesn't provide a service of archiving files.");
            return false;
        }
        
        name = userName;
        
        try{ 
    		localFiles  = new HashSet<String>();
    		if(authentication(userPass)){
    			printInfo("User " + userName + " has been successfully logged in.");
    			clientSecurity.initCipherKey(userPass);
    			logFlag = true;
    			return true;
    		}else{
    			printInfo("Invalid data for " + userName + ".");
    			logFlag = false;
    			return false;
    		}
        }catch (Exception e){
        	printInfo("Server error");
        	logFlag = false;
            return false;
        }        
   	}
	
	/**
	 * Metoda wylogowania uzytkownika z serwera.
	 */	
	boolean logOff(){
		try{
			Long timestamp = new Date().getTime();
			byte[] msg = timestamp.toString().getBytes();
			byte[] cipheredMsg = clientSecurity.cipherMessage(msg, Cipher.ENCRYPT_MODE);
			
			if(!server.logOff(name, cipheredMsg))
				throw new Exception();
	
		} catch (Exception e){
			printInfo("Server error.");
			return false;
		}
		
		logFlag = false;
		localFiles = null;
		printInfo("Logged out.");
		return true;
	}
	
	/**
	 * Metoda dodajaca nowy plik przeznaczony do archiwizacji.
	 */	
	boolean addLocalFile(String f){
		boolean result;
		if(result=localFiles.add(f)) 
			clientView.updateLocalTree(localFiles);
		return result;
	}
	
	/**
	 * Metoda usuwajaca plik przeznaczony do archiwizacji.
	 */	
	void removeLocalFile(HashSet<String> f){
		Iterator<String> i = f.iterator();
		while(i.hasNext())
			localFiles.remove(i.next()); 
		clientView.updateLocalTree(localFiles);
	}
	
	/**
	 * Metoda uaktualniajaca liste zarchiwizowanych plikow w widoku drzewa
	 */
		
	public void updateRemoteTree()
	{
		try {
			GetFilesMessage message = new GetFilesMessage(name, new Date().getTime());
			byte[] msg = SerializationUtils.serialize(message);
			byte[] cipheredMsg = clientSecurity.cipherMessage(msg, Cipher.ENCRYPT_MODE);
			
			byte[] treemsg = server.getFileList(cipheredMsg, this);
			remoteFiles = SerializationUtils.deserialize(clientSecurity.cipherMessage(treemsg, Cipher.DECRYPT_MODE));
			
			if(remoteFiles==null)
				throw new Exception();
			
			clientView.updateRemoteTree(remoteFiles);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}
	
	
	/**
	 * Metoda wykonujaca wysylanie wybranych plikow na serwer
	 */
	void sendFilesToServer(){

		ArrayList<File> files = new ArrayList<File>();
		Iterator<String> i = localFiles.iterator();
		while(i.hasNext())
			files.add(new File(i.next()));
		
		if(!files.isEmpty()){
			for(int iter = 0; iter < files.size(); ++iter)
			{
				clientView.updateProgressBar(0);
				String filepath = files.get(iter).getPath();
				
				int result;
				String checksum;
				byte[] cipherFile;
				
				try {
					
					FileInputStream fileInputStream = new FileInputStream(filepath);
					cipherFile = clientSecurity.cipherFile(fileInputStream, Cipher.ENCRYPT_MODE);
					checksum = countChecksum(filepath, cipherFile);
					fileInputStream.close();
					
					CheckFileMessage message = new CheckFileMessage(name, filepath, checksum, false, new Date().getTime());
					byte[] msg = SerializationUtils.serialize(message);
					byte[] cipheredMsg = clientSecurity.cipherMessage(msg, Cipher.ENCRYPT_MODE);
					
					result = (int) server.checkFileOnServer(cipheredMsg, this);
					
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
				
				switch (result)
				{
				case 0:
					clientView.printInfo("Sending a file: " + filepath + " to a server, no previous version in an archive.");
					break;
				case -1:
					clientView.printInfo("Sending a file: " + filepath + " to a server, previous versions have been found in an archive.");					
					break;
				case -2:
					clientView.printInfo("No need to send a file: " + filepath + " to a server, the newest version is already on a server.");	
					continue;
				default:
					clientView.printInfo("Error in checking a file: " + filepath + " on a server.");
					continue;
				}
	
				SimpleRemoteInputStream istream = null;
				int sizeCipher;
				
				try {
					sizeCipher = cipherFile.length;	
					byte[] encryptedFile = clientSecurity.cipherMessage(cipherFile, Cipher.ENCRYPT_MODE);
					istream = new SimpleRemoteInputStream(new ByteArrayInputStream(encryptedFile));
					
				} catch (Exception e) {
		            System.err.println("File not found, " + e);
		            continue;
				}
				
				try {
					try {
						ReceiveFileMessage message = new ReceiveFileMessage(name, filepath, checksum, sizeCipher, new Date().getTime());
						byte[] msg = SerializationUtils.serialize(message);
						byte[] cipheredMsg = clientSecurity.cipherMessage(msg, Cipher.ENCRYPT_MODE);
						if(server.receiveFileFromClient(cipheredMsg, istream.export(), this) != 0)
							throw new Exception();
					} catch (Exception e) {
						clientView.printInfo("Error in sending a file " + filepath + " to a server.");
						clientView.updateProgressBar(0);
						continue;
					} 
				} finally {
					istream.close();
				}
				
				clientView.printInfo("Sending a file " + filepath + " to a server has been finished.");
			}
			
			updateRemoteTree();
		}
	}
	
	/**
	 * Metoda pobierajaca okreslone wersje archiwizowanych plikow z serwera
	 * @param files - lista plikow do pobrania
	 * @throws IOException
	 */
	
	public void getFilesFromServer(ArrayList<String> files) throws IOException
	{
		if(files.isEmpty())
			return;

		for(String fileName : files)
		{
			clientView.updateProgressBar(0);
			String tmp = fileName;
			if(fileName.contains("|"))
			{
				int start = fileName.indexOf("|");
				int end = fileName.length();
				tmp = new StringBuilder(fileName).replace(start, end, "").toString();
			}
			
			File file = new File(tmp);
			
			if(file.exists())
			{	
				String checksum;
				int result;
				
				try {
					
					FileInputStream fileInputStream = new FileInputStream(tmp);
					byte[] cipherFile = clientSecurity.cipherFile(fileInputStream, Cipher.ENCRYPT_MODE);
					checksum = countChecksum(tmp, cipherFile);
					fileInputStream.close();
					
					CheckFileMessage message = new CheckFileMessage(name, fileName, checksum, false, new Date().getTime());
					byte[] msg = SerializationUtils.serialize(message);
					byte[] cipheredMsg = clientSecurity.cipherMessage(msg, Cipher.ENCRYPT_MODE);
					
					result = (int) server.checkFileOnServer(cipheredMsg, this);
					
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}	
				
				switch (result)
				{
				case -1:
					clientView.printInfo("Loading file: " + tmp + " from server, an archived file is diffrent than a local one.");					
					break;
				case -2:
					clientView.printInfo("No need to load a file: " + tmp + " from a server, an archived and local files are the same.");	
					continue;
				default:
					clientView.printInfo("Error in calculating a checksum of " + tmp + " on a server.");
					continue;
				}
				
				if(!file.delete())
				{
					printInfo("Error in deleting a file " + tmp + " from a local disk, check privileges.");
				}
				
			}
			else
				printInfo("A local file doesn't exist, downloading the file from a server.");

			FileOutputStream ostream = null;
			InputStream istreamEncrypted = null;
			InputStream istreamCipher = null;
			InputStream istream = null;
			
			try {
				CheckFileMessage message1 = new CheckFileMessage(name, fileName, "", true, new Date().getTime());
				byte[] msg = SerializationUtils.serialize(message1);
				byte[] cipheredMsg = clientSecurity.cipherMessage(msg, Cipher.ENCRYPT_MODE);
				
				long fileSize = server.checkFileOnServer(cipheredMsg, this);
				if(fileSize < 0)
					throw new Exception();
				
				SendFileMessage message2 = new SendFileMessage(name, fileName, new Date().getTime());
				msg = SerializationUtils.serialize(message2);
				cipheredMsg = clientSecurity.cipherMessage(msg, Cipher.ENCRYPT_MODE);
				
				istreamEncrypted = RemoteInputStreamClient.wrap(server.sendFileToClient(cipheredMsg, this));
				byte[] cipherFile = clientSecurity.cipherMessage(IOUtils.toByteArray(istreamEncrypted), Cipher.DECRYPT_MODE);
				istreamCipher = new ByteArrayInputStream(cipherFile);
				istream = new ByteArrayInputStream(clientSecurity.cipherFile(istreamCipher, Cipher.DECRYPT_MODE));

				file.createNewFile();
				ostream = new FileOutputStream(file);
				byte[] buf = new byte[1024];
				
				int bytesRead = 0;
				int counter = 0;			
				int currentPercentValue = 0;
				int onePercentValue = (int) Math.ceil(((double)fileSize)/(1024 * 100));
				
				while((bytesRead = istream.read(buf)) >= 0)
				{
					ostream.write(buf, 0, bytesRead);
					if(++counter % onePercentValue == 0)
					{
						updateProgressBar(currentPercentValue++);
					}
				}
				ostream.flush();
				printInfo("File " + tmp + " has been downloaded.");
			
			} catch (Exception e) {
				printInfo("Error in downloading a file " + tmp + " from a server.");
				if(file != null && file.exists())
					file.delete();
				printInfo(e.toString());
	
			} finally {
				if(istreamEncrypted != null)
					istreamEncrypted.close();
				if(istreamCipher != null)
					istreamCipher.close();
				if(istream != null)
					istream.close();
				if(ostream != null)
					ostream.close();
				updateProgressBar(100);
			}
			
		}
		
	}
	
	
	/**
	 * Metoda usuwajaca wybrane zarchiwizowane pliki (badz ich konkretne wersje) z serwera
	 * @param files
	 */
	public void removeFilesFromServer(ArrayList<String> files)
	{
		if(files.isEmpty())
			return;
		
		try {
			RemoveFilesMessage message = new RemoveFilesMessage(name, files, new Date().getTime());
			byte[] msg = SerializationUtils.serialize(message);
			byte[] cipheredMsg = clientSecurity.cipherMessage(msg, Cipher.ENCRYPT_MODE);
			
			if(server.removeFilesFromServer(cipheredMsg, this) != 0)
				throw new Exception();
			
			updateRemoteTree();
		} catch (Exception e) {
			printInfo("Error in removing a file from a server.");
			printInfo(e.toString());
		}
		
	}
	
	/**
	 * Metoda wyliczajaca sume kodowa lokalnego pliku przy wykorzystaniu algorytmu SHA1
	 * @param filename - sciezka pliku z jego nazwa
	 * @return - suma kodowa
	 * @throws Exception
	 */
	
	String countChecksum(String filename, byte[] stream) throws Exception
	{
		printInfo("Calculating SHA-1 for a local file: " + filename + ".");
		MessageDigest md = MessageDigest.getInstance("SHA1");
		
	    InputStream inputstream = new ByteArrayInputStream(stream);
		byte[] dataBytes = new byte[1024];
	 
	    int nread = 0; 
	 
	    while ((nread = inputstream.read(dataBytes)) != -1) {
	      md.update(dataBytes, 0, nread);
	    };
	    byte[] mdbytes = md.digest();
	 
	    StringBuffer sb = new StringBuffer("");
	    for (int i = 0; i < mdbytes.length; i++) {
	    	sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
	    }
	    
	    inputstream.close();
		return sb.toString();
	}
	
	
	/**
	 * Metoda uaktualniajaca progress bar ze stanem pobierania pliku
	 */
	public void updateProgressBar(int percentage) throws RemoteException {
		clientView.updateProgressBar(percentage);
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 *Metoda prezentujaca na konsoli informacje. 
	 */
	void printInfo(String info){
		clientView.printInfo(info);
	}

	/**
	 * Glowna metoda klasy.
	 * @param args
	 * @throws RemoteException 
	 */
	public static void main(String args[]) throws RemoteException {
        try {
            System.setSecurityManager(new RMISecurityManager());
        } catch (SecurityException e) {
            System.err.println("Security violation " + e);
            System.exit(1);
        }
		new FBClient();
	}
	
	private boolean authentication(String pass){
		try {
			byte[] message = clientSecurity.authInitArguments(pass);
			return server.authInit(name, message, this);
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} 
	}
	
	public boolean exchangeSessionKey(byte[] message) throws RemoteException{		
		try {
			clientSecurity.obtainSessionKey(message);
			byte[] encryptedChallenge = clientSecurity.generateChallenge();
			return server.challenge(name, encryptedChallenge, this);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}	
	}
	
	public boolean challenge(byte[] encryptedChalange) throws RemoteException{	
		try {
			byte[] encryptedChallenge = clientSecurity.respond2Challenge(encryptedChalange);
			return server.authFinish(name, encryptedChallenge);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public String getName() throws RemoteException {
		return name;
	}
}
