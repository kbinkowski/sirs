package FBClient;

import java.rmi.Remote;
import java.rmi.RemoteException;


/**
 * Klasa zawierajaca metody udostepniane serwerowi przy pomocy rmi
 * @author F-B team
 */
public interface FBClientInterface extends Remote {
	void updateProgressBar(int percentage) throws RemoteException;
	boolean exchangeSessionKey(byte[] message) throws RemoteException;
	boolean challenge(byte[] encryptedChalange) throws RemoteException;
	String getName() throws RemoteException;
}
