package FBClient;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;


/**
 * Klasa reprezentujca okno glowne klienta uslugi archiwizatora.
 * @author F-B team
 */
public class FBClientView extends JFrame implements ActionListener, TreeSelectionListener{
	private static final long serialVersionUID = 1L;

	/**
	 * Pole reprezentujace aktualnie zalogowanego klienta. 
	 */
	FBClient client;
	
	/**
	 * Kontener zawierajacy stan przyciskow przed wcisnieciem przyciskow sendFile i loadFile
	 */
	HashMap<String, Boolean> valuesOfButtons;
	
	/**
	 * Kontener zawierajacy mape archiwizowanych plikow na serwerze
	 */
	HashMap<String, ArrayList<String> > remoteFileMap;
	
	/**
	 * Kontener zawierajacy liste plikow zaznaczonych do usuniecia
	 */		
	ArrayList<String> filesToDelete;

	/**
	 * Kontener zawierajacy liste plikow zaznaczonych do pobrania z serwera
	 */		
	ArrayList<String> filesToLoad;	

	/**
	 * Runnable do wysylania plikow na serwer 
	 */
	public class InternalSendingRunnable implements Runnable
	{
		public void run() {
			updateEnabledTree(false);
			disableAllButtons();
			client.sendFilesToServer();
			restoreButtons();
			updateEnabledTree(true);
			logArea.append("Files have been sent to an archive.\n");
		}
	}

	
	/**
	 * Runnable do usuwania zarchiwizowanych plikow z serwera
	 */	
	public class InternalRemovingRunnable implements Runnable
	{
		public void run() {
			updateEnabledTree(false);
			disableAllButtons();
			client.removeFilesFromServer(filesToDelete);
			restoreButtons();
			updateEnabledTree(true);
			logArea.append("Files have been deleted from an archive.\n");
		}
	}
	
	/**
	 * Runnable do pobierania plikow z serwera
	 */
	public class InternalLoadingRunnable implements Runnable
	{
		public void run() {
			updateEnabledTree(false);
			disableAllButtons();
			try {
				client.getFilesFromServer(filesToLoad);
			} catch (IOException e) {
				e.printStackTrace();
			}
			restoreButtons();
			updateEnabledTree(true);
			logArea.append("Files have been downloaded from an archive.\n");
		}
	}
	
	
	/**
	 * Menager wyboru lokalnego pliku do archiwizacji.
	 */
	JFileChooser fileChooser;
	
	/**
	 * Pole wyboru serwera.
	 */
	private JTextField server;
	
	/**
	 * Pole nazwy uzytkownika.
	 */
	private JTextField userName;
	
	/**
	 * Pole hasla uzytkownika.
	 */
	private JPasswordField userPassword;
	
	/**
	 * Przycisk odpowiadajacy za polaczenie z serwerem. 
	 */
	private JButton connect;
	
	/**
	 * Przycisk odpowiadajacy za rozlaczenie z serwerem.
	 */
	private JButton disconnect;
	
	/**
	 * Drzewo lokalnych plikow przeznaczonych do archiwizacji.
	 */
	private JTree localTree;
	
	/**
	 * Korzen drzewa lokalnych plikow przeznaczonych do archiwizacji.
	 */
	private DefaultMutableTreeNode localTreeNode;
	
	/**
	 * Drzewo zdalnych plikow w archiwum.
	 */
	private JTree remoteTree;
	
	/**
	 * Korzen drzewa zdalnych plikow w archiwum.
	 */
	private DefaultMutableTreeNode remoteTreeNode;
	
	/**
	 * Przycisk odpowiadajacy za dodanie nowego pliku do archiwizacji.
	 */
	private JButton addLocalFile;
	
	/**
	 * Przycisk odpowiadajacy za usuniecie pliku do archiwizacji.
	 */
	private JButton removeLocalFile;
	
	/**
	 * Przycisk odpowiadajacy za rozpoczecie przesylania plikow do archiwizacji.
	 */
	private JButton sendFile;	
	
	/**
	 * Przycisk odpowiadajacy za usuniecie pliku archiwizowanego.
	 */
	private JButton removeRemoteFile;
	
	/**
	 * Przycisk odpowiadajacy za rozpoczecie pobierania plikow archiwizowanych.
	 */
	private JButton loadFile;
	
    /**
     * Pole sluzace do prezentacji informacji o stanie aplikacji.
     */
    private TextArea logArea;
    
    /**
     * Pole sluzace do prezentacji informacji o wyniku przesylania pliku.
     */
    private JProgressBar progressBar;
  
    /**
	 * Konstruktor okna aplikacji klienta usugi archiwizatora.
	 */
	FBClientView(FBClient client){
		super("FileBackuper");
		this.client = client;
		setMinimumSize(new Dimension(800,600));
		init();
		pack();
		setVisible(true);
		addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent we) { System.exit(0); } }); 
	}  
	
	/**
	 * Metoda inicjujaca okno aplikacji klienta.
	 */
	private void init(){
		//Stworzenie komponentow skladowych
		createComponent();
		
		//Glowne panele okna
		GridLayout gridLayout = new GridLayout(1,2);
		gridLayout.setHgap(100);
		gridLayout.setVgap(20);
		JPanel logging = new JPanel(gridLayout);
		logging.setBorder(BorderFactory.createTitledBorder("Logging"));
		JPanel backuper = new JPanel(new BorderLayout());
		backuper.setBorder(BorderFactory.createTitledBorder("Backuper"));
		JPanel logs = new JPanel(new BorderLayout());
		logs.setBorder(BorderFactory.createTitledBorder("State"));
        setLayout(new BorderLayout());
        add(logging, BorderLayout.NORTH);
        add(backuper, BorderLayout.CENTER);
        add(logs, BorderLayout.SOUTH);
        
        //Panel logowania
        JPanel infoPanel = new JPanel(new BorderLayout());
        JPanel commandInfoPanel = new JPanel(new GridLayout(3,1)); 
        JPanel textInfoPanel = new JPanel(new GridLayout(3,1)); 
        commandInfoPanel.add(new JLabel("    Server:    "));
        commandInfoPanel.add(new JLabel("    Username:    "));
        commandInfoPanel.add(new JLabel("    Password:    "));
        textInfoPanel.add(server);
        textInfoPanel.add(userName);
        textInfoPanel.add(userPassword);
        infoPanel.add(commandInfoPanel, BorderLayout.WEST);
        infoPanel.add(textInfoPanel, BorderLayout.CENTER);
        JPanel infoButtonPanel = new JPanel(new GridLayout(2,1));
        infoButtonPanel.add(connect);
        infoButtonPanel.add(disconnect);          
        logging.add(infoPanel);
        logging.add(infoButtonPanel);
        
        //Panel statusu
        logs.add(logArea, BorderLayout.CENTER);
        logs.add(progressBar, BorderLayout.SOUTH);
        
        //Panel backupera
        GridLayout gridLayout2 = new GridLayout(1,8);
        gridLayout2.setHgap(10);
        JPanel buttonPanel = new JPanel(gridLayout2);
        buttonPanel.add(addLocalFile);
        buttonPanel.add(removeLocalFile);
        buttonPanel.add(new JLabel());
        buttonPanel.add(sendFile);
        buttonPanel.add(loadFile);
        buttonPanel.add(new JLabel());
        buttonPanel.add(new JLabel());
        buttonPanel.add(removeRemoteFile);
        backuper.add(buttonPanel, BorderLayout.SOUTH);
        JPanel treesPanel = new JPanel(new GridLayout(1,2));
        JScrollPane localTreePane = new JScrollPane(localTree);
        JScrollPane remoteTreePane = new JScrollPane(remoteTree);
        treesPanel.add(localTreePane);
        treesPanel.add(remoteTreePane);
       
        backuper.add(treesPanel, BorderLayout.CENTER);
        
    }  
	
	/**
	 * Metoda tworzaca komonenty skladowe okna aplikacji klienta.
	 */
	private void createComponent(){
		 fileChooser = new JFileChooser();
		 server = new JTextField("rmi://127.0.0.1:1099/backuper");
	     userName = new JTextField();
	     userPassword = new JPasswordField();
	     connect = new JButton("Connect");
	     connect.addActionListener(this);
	     disconnect = new JButton("Disconnect");
	     disconnect.addActionListener(this);
	     logArea =new TextArea();
	     logArea.setEditable(false);
	     progressBar = new JProgressBar();
	     localTreeNode = new DefaultMutableTreeNode("Computer");
	     localTree = new JTree(localTreeNode);
	     remoteTreeNode = new DefaultMutableTreeNode("Archive");
	     remoteTree = new JTree(remoteTreeNode);
	     localTree.addTreeSelectionListener(this);
	     remoteTree.addTreeSelectionListener(this);
	     addLocalFile = new JButton("Add");
	     addLocalFile.addActionListener(this);
	     removeLocalFile = new JButton("Delete");
	     removeLocalFile.addActionListener(this);
	     sendFile = new JButton("Archive");
	     sendFile.addActionListener(this);
	     removeRemoteFile = new JButton("Delete");
	     removeRemoteFile.addActionListener(this);
	     loadFile = new JButton("Download");
	     loadFile.addActionListener(this);
	     
	     //Set enabled of component
	     updateEnabledButton();
	     updateEnabledLogging(true);
	     updateEnabledTree(false);
	     updateProgressBar(0);
	}
	
	
	/**
	 * Metoda prezentujaca na konsoli informacje.
	 */
	void printInfo(String info){
		logArea.append(info+"\n");
	}
	
	/**
	 * Metoda aktualizujaca stan przyciskow.
	 */
	void updateEnabledButton(){
		
		if(localTree.isSelectionEmpty())
			removeLocalFile.setEnabled(false);
		else
			removeLocalFile.setEnabled(true);
		
		if(localTreeNode.isLeaf()) 
			sendFile.setEnabled(false);
		else 
			sendFile.setEnabled(true);
		
		if(remoteTree.isSelectionEmpty())
		{
			removeRemoteFile.setEnabled(false);
			loadFile.setEnabled(false);
		}
		else
		{
			removeRemoteFile.setEnabled(true);
			loadFile.setEnabled(true);
		}
		if(client.isLogOn()){
			connect.setEnabled(false);
			addLocalFile.setEnabled(true);
			disconnect.setEnabled(true);
		}
		else{
			connect.setEnabled(true);
			addLocalFile.setEnabled(false);
			disconnect.setEnabled(false);
			sendFile.setEnabled(false);
			loadFile.setEnabled(false);
			removeRemoteFile.setEnabled(false);
		}
	
	}
	
	void disableAllButtons()
	{
		valuesOfButtons = new HashMap<String, Boolean>();
		valuesOfButtons.put("connect", connect.isEnabled());
		valuesOfButtons.put("disconnect", disconnect.isEnabled());
		valuesOfButtons.put("addLocalFile", addLocalFile.isEnabled());
		valuesOfButtons.put("removeLocalFile", removeLocalFile.isEnabled());
		valuesOfButtons.put("sendFile", sendFile.isEnabled());
		valuesOfButtons.put("removeRemoteFile", removeRemoteFile.isEnabled());
		valuesOfButtons.put("loadFile", loadFile.isEnabled());		
		
		connect.setEnabled(false);
		disconnect.setEnabled(false);
		addLocalFile.setEnabled(false);
		removeLocalFile.setEnabled(false);
		sendFile.setEnabled(false);
		removeRemoteFile.setEnabled(false);
		loadFile.setEnabled(false);
				
	}
	
	void restoreButtons()
	{
		for(int i =100; i >= 0; --i)
		{
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			updateProgressBar(i);
		}
		
		connect.setEnabled(valuesOfButtons.get("connect"));
		disconnect.setEnabled(valuesOfButtons.get("disconnect"));
		addLocalFile.setEnabled(valuesOfButtons.get("addLocalFile"));
		removeLocalFile.setEnabled(valuesOfButtons.get("removeLocalFile"));
		sendFile.setEnabled(valuesOfButtons.get("sendFile"));
		removeRemoteFile.setEnabled(valuesOfButtons.get("removeRemoteFile"));
		loadFile.setEnabled(valuesOfButtons.get("loadFile"));
		
	}
	
	
	/**
	 * Metoda ustawiajaca mozliwosc edytowania pol identyfikujacych serwer i uzytkownika.
	 */
	void updateEnabledLogging(boolean choice){
		server.setEnabled(choice);
		userName.setEnabled(choice);
		userPassword.setEnabled(choice);
		connect.setEnabled(choice);
		disconnect.setEnabled(!choice);
	}
	
	/**
	 * Metoda ustawiajaca mozliwosc edycji drzew plikow.
	 */
	void updateEnabledTree(boolean choice){
		localTree.setEnabled(choice);
		remoteTree.setEnabled(choice);
	}
	
	/**
	 * Metoda aktualizujaca stan pasku postepu.
	 */
	void updateProgressBar(int i){

		synchronized(progressBar) {
			progressBar.setValue(i);		
			progressBar.repaint();
		}
	}
	
	
	/**
	 * Metoda aktualizujaca drzewo plikow zarchiwizowanych.
	 */		
	void updateRemoteTree(HashMap<String, ArrayList<String> > remoteMap){ 
		
		remoteFileMap = remoteMap;
		remoteTree.clearSelection();
		remoteTree.expandRow(0);
		remoteTreeNode.removeAllChildren();


		if(remoteMap == null || remoteMap.isEmpty())
		{
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			remoteTree.clearSelection();
			remoteTree.updateUI();
			return;
		}

		SortedSet<String> keys = new TreeSet<String>(remoteMap.keySet());
		for (String key : keys) { 
			   ArrayList<String> values = remoteMap.get(key);
			   DefaultMutableTreeNode node = new DefaultMutableTreeNode(key);
			   remoteTreeNode.add(node);
			   for(String value : values)
				   node.add(new DefaultMutableTreeNode(value.replace(";", ":")));
		}
		
	    remoteTree.expandRow(0);
	    remoteTree.clearSelection();

		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    remoteTree.updateUI();	

	}
	
	
	
	/**
	 * Metoda aktualizujaca drzewo plikow do archiwizacji.
	 */	
	void updateLocalTree(HashSet<String> localFiles){
		localTreeNode.removeAllChildren();
		if(localFiles != null)
		{
			Iterator<String> i = localFiles.iterator();
			while(i.hasNext()){
				DefaultMutableTreeNode node = new DefaultMutableTreeNode(i.next());
				localTreeNode.add(node);
			}
		}
		localTree.clearSelection();

		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		localTree.updateUI();
	}

	/**
	 * Reakcja na wcisniecie przyciskow
	 */	
	public void actionPerformed(ActionEvent e){
		if (e.getSource() == connect){
			
			try{
				String serv = server.getText();
				String name = userName.getText();
				String pass = userPassword.getText();
				if(name.equals("") || pass.equals("") || serv.equals("")){
					printInfo("Please, fill in all the fields.");
				}
				else if(name.contains("\\")||name.contains("/")||name.contains(":")||name.contains("*")||
						name.contains("?")||name.contains("\"")||name.contains("<")||name.contains(">")||name.contains("|")){
					printInfo("Do not use special characters.");
				}
				else{
					if(client.logOn(serv, name, pass)){                   
						updateEnabledLogging(false);
						updateEnabledTree(true);
					} 
				}
			}catch (Exception p){
				logArea.append("Error. Invalid username or password.\n");
			}
			if(client.isLogOn())
				client.updateRemoteTree();
			
		}else if (e.getSource() == disconnect){
			
			 try{
				 updateLocalTree(null);
				 updateRemoteTree(null);
    			 if (client.logOff()){
    				 updateEnabledLogging(true);
    				 updateEnabledTree(false);
    				 logArea.append("Conncetion with a server has been closed.\n");
    			 } else
    				 logArea.append("Error in disconnecting with a server.\n");
    		 }catch (Exception p){
    			 logArea.append("Error in disconnecting with a server.\n");
    		 }
			 			 
		}else if (e.getSource() == addLocalFile){
			
   		 	try{
   		 		int returnValue = fileChooser.showOpenDialog(this);
   		 		if (returnValue == JFileChooser.APPROVE_OPTION){
   		 			File file = fileChooser.getSelectedFile();
   		 			if(client.addLocalFile(file.getPath())){
   		 				logArea.append("File with a name: " + file.getName() + " has been added.\n");
   		 			}
   		 			else
   		 				logArea.append("File with a given path has already been added to a list of archived files.\n");
   		 		} 
   		 	}catch (Exception p){
   		 		logArea.append("Error in adding a file.\n");
   		 	}
   		 	
		}else if (e.getSource() == removeLocalFile){
			
			HashSet<String> files = new HashSet<String>();
			int[] rows = localTree.getSelectionRows();
			
			if (rows[0]>0){
				for(int i=0; i<rows.length; ++i)
					files.add(localTreeNode.getChildAt(rows[i]-1).toString());
			}
			else {
				for(int i=0; i<localTreeNode.getChildCount(); ++i)
					files.add(localTreeNode.getChildAt(i).toString());
			}
			client.removeLocalFile(files);
			logArea.append("Files have been removed from a list of fles to archive.\n");
			
		}else if (e.getSource() == sendFile){

			remoteTree.clearSelection();
			localTree.clearSelection();
			Thread sendingThread = new Thread(new InternalSendingRunnable());
			sendingThread.start();
						
		}else if (e.getSource() == loadFile){
			
			filesToLoad = new ArrayList<String>();
			
			int[] rows = remoteTree.getSelectionRows();
			
			if(rows[0]>0) {
				
				TreePath[] paths = remoteTree.getSelectionPaths();
				
				SortedSet<String> keys = new TreeSet<String>(remoteFileMap.keySet());
				
				for(TreePath path : paths) {
					
					String filePath = path.toString();
					String fileToLoad = "";
					
					for (String key : keys)
					{
						if(filePath.contains(key))
						{
							ArrayList<String> fileVersion = remoteFileMap.get(key);
							
							for(String version : fileVersion)
							{
								if(filePath.contains(version.replace(";", ":") + "]"))
								{
									fileToLoad = key + "|" + version;
									break;
								}
							}	
							
							if(fileToLoad.equals(""))
								fileToLoad = key;
							break;
						}
					}
					filesToLoad.add(fileToLoad);
				}
				
			} else {
				printInfo("Loading newest versions of files!");
				for(int i=0; i < remoteTreeNode.getChildCount(); ++i) {
					filesToLoad.add(remoteTreeNode.getChildAt(i).toString());
					logArea.append(remoteTreeNode.getChildAt(i).toString() + "\n");
				}
			}

			remoteTree.clearSelection();
			localTree.clearSelection();
			Thread loadingThread = new Thread(new InternalLoadingRunnable());
			loadingThread.start();
			
		} else if (e.getSource() == removeRemoteFile){
			
			filesToDelete = new ArrayList<String>();
			
			int[] rows = remoteTree.getSelectionRows();
			
			if(rows[0]>0) {
				
				TreePath[] paths = remoteTree.getSelectionPaths();
				
				SortedSet<String> keys = new TreeSet<String>(remoteFileMap.keySet());
				
				for(TreePath path : paths) {
					
					String filePath = path.toString();
					String fileToDelete = "";
					
					for (String key : keys)
					{
						if(filePath.contains(key))
						{
							ArrayList<String> fileVersion = remoteFileMap.get(key);
						
							for(String version : fileVersion)
							{
								if(filePath.contains(version.replace(";", ":") + "]"))
								{
									fileToDelete = key + "|" + version;
									break;
								}
							}	
							
							if(fileToDelete.equals(""))
								fileToDelete = key;
							
							break;
						}
					}
					filesToDelete.add(fileToDelete);
				}
				
			} else {
				for(int i=0; i < remoteTreeNode.getChildCount(); ++i)
					filesToDelete.add(remoteTreeNode.getChildAt(i).toString());
			}

			remoteTree.clearSelection();
			localTree.clearSelection();
			Thread removingThread = new Thread(new InternalRemovingRunnable());
			removingThread.start();
		}
		updateEnabledButton();
	}
	
	 public void valueChanged(TreeSelectionEvent e) {
		 updateEnabledButton();
 	 }

}
