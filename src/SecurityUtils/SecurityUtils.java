package SecurityUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class SecurityUtils {

	public static KeyPair generateKeyPair(String alg, int size)
			throws NoSuchAlgorithmException, NoSuchProviderException {
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance(alg);
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
		keyGen.initialize(size, random);
		return keyGen.generateKeyPair();
	}
	
	public static SecretKey generateSecretKey(String alg, int size) 
			throws NoSuchAlgorithmException, NoSuchProviderException{
		KeyGenerator keyGen = KeyGenerator.getInstance(alg);	
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
		keyGen.init(size, random);
		return keyGen.generateKey();
	}
	
	public static SecretKey convertHash2Key(String alg, byte[] hash) {
		return new SecretKeySpec(hash, 0, hash.length, alg);
	}
	

	public static byte[] hashArray(String alg, byte[] array)
			throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(alg);
		return md.digest(array);
	}

	public static byte[] hashStream(String alg, InputStream inputstream)
			throws IOException, NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(alg);
		byte[] dataBytes = new byte[1024];
		int nread = 0;
		while ((nread = inputstream.read(dataBytes)) != -1) {
			md.update(dataBytes, 0, nread);
		}
		;
		return md.digest();
	}

	public static void cipherStream(String alg, Key key, int mode,
			InputStream inputstream, OutputStream outputstream)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IOException, IllegalBlockSizeException,
			BadPaddingException {
		Cipher cipher = Cipher.getInstance(alg);
		cipher.init(mode, key);
		byte[] dataBytes = new byte[1024];
		int nread = 0;
		while ((nread = inputstream.read(dataBytes)) != -1) {
			outputstream.write(cipher.update(dataBytes, 0, nread));
		}
		outputstream.write(cipher.doFinal());
	}
	
	public static byte[] cipherArray(String alg, Key key, int mode,
			byte[] data)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IOException, IllegalBlockSizeException,
			BadPaddingException {
		ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		Cipher cipher = Cipher.getInstance(alg);
		cipher.init(mode, key);
		byte[] dataBytes = new byte[1024];
		int nread = 0;
		while ((nread = inputStream.read(dataBytes)) != -1) {
			outputStream.write(cipher.update(dataBytes, 0, nread));
		}
		outputStream.write(cipher.doFinal());
		byte[] result = outputStream.toByteArray();
		outputStream.close();
		inputStream.close();
		return result;
	}

}
