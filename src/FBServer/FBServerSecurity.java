package FBServer;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import SecurityUtils.SecurityUtils;


public class FBServerSecurity {
	
	class SecurityInformation {
		SecretKey keySession;
		int challange;
		long timestamp;
		
		SecurityInformation(SecretKey key){
			keySession = key;
			timestamp = 0;
		}
		void setKeySession(SecretKey key){
			keySession = key;
		}
		void setChallange(int ch){
			challange = ch;
		}
		void setTimestamp(long st){
			timestamp = st;
		}
		SecretKey getKeySession(){
			return keySession;
		}
		int getChallange(){
			return challange;
		}
		long getTimestamp(){
			return timestamp;
		}
	}
	
	final long timeframe = 30000; //test
	
	ConcurrentMap<String, SecurityInformation> sessionInformation = new ConcurrentHashMap<String, SecurityInformation>();
	
	public byte[] generateKeySession(byte[] hashPassword,
			byte[] message, String name) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, IOException,
			InvalidKeySpecException, NoSuchProviderException {

		SecretKey passwordKey = SecurityUtils.convertHash2Key("AES",
				hashPassword);

		byte[] publicKeyBytes = SecurityUtils.cipherArray("AES", passwordKey,
				Cipher.DECRYPT_MODE, message);
		PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(
				new X509EncodedKeySpec(publicKeyBytes));
		SecretKey sessionKey = SecurityUtils.generateSecretKey("AES", 256);
		
		sessionInformation.put(name, new SecurityInformation(sessionKey));

		byte[] encryptedSessionKey = SecurityUtils.cipherArray("RSA",
				publicKey, Cipher.ENCRYPT_MODE, sessionKey.getEncoded());
		return SecurityUtils.cipherArray("AES", passwordKey,
				Cipher.ENCRYPT_MODE, encryptedSessionKey);

	}

	public byte[] respond2Challenge(String login,
			byte[] encryptedChallenge) throws Exception {
		SecretKey sessionKey = sessionInformation.get(login).getKeySession();
		byte[] clientChallenge = SecurityUtils.cipherArray("AES", sessionKey,
				Cipher.DECRYPT_MODE, encryptedChallenge);
		Random generator = new Random(System.currentTimeMillis());
		Integer challenge = generator.nextInt();
		sessionInformation.get(login).setChallange(challenge);
		String messageToEncrypt = new String(clientChallenge) + "|"
				+ challenge.toString();
		return SecurityUtils.cipherArray("AES", sessionKey,
				Cipher.ENCRYPT_MODE, messageToEncrypt.getBytes());
	}

	public boolean verifyChallenges(String login,
			byte[] encryptedChallenge) throws Exception {
		SecretKey sessionKey = sessionInformation.get(login).getKeySession();
		byte[] challengeNumber = SecurityUtils.cipherArray("AES", sessionKey,
				Cipher.DECRYPT_MODE, encryptedChallenge);
		if (Integer.parseInt(new String(challengeNumber)) == sessionInformation.get(login).getChallange()) {
			return true;
		}
		return false;

	}
	
	public boolean checkTimestamp(String login, long timestamp){
		if( new Date().getTime() <= timestamp + timeframe )
			if( timestamp > sessionInformation.get(login).getTimestamp() ){
				sessionInformation.get(login).setTimestamp(timestamp);
				return true;
			}
		return false;				
	}

	public byte[] cipherMessage(byte[] msg, String login, int mode) {
		try {
			return SecurityUtils.cipherArray("AES", sessionInformation.get(login).getKeySession(),
					mode, msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
