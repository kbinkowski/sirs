package FBServer;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;




/**
 * Klasa reprezentujca okno glowne serwera usugi archiwizatora.
 * @author F-B team
 */
public class FBServerView extends JFrame{ 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 123L;

	/**
	 * Pole reprezentujace serwer aplikacji archiwizacji plikow. 
	 */
	private FBServer server;
	
    /**
     * Pole sluzace do prezentacji informacji o stanie aplikacji.
     */
    private TextArea logArea;
    
	/**
	 * Pola logowania do bazy danych.
	 */
    JTextField databaseURL;
    JTextField databaseUser;
    JPasswordField databasePass;
    JButton databaseConnect;
    
	/**
	 * Konstruktor okna aplikacji serwera usugi archiwizatora.
	 */
	FBServerView(FBServer server){
		super("FileBackuper Server");
		this.server = server;
		setMinimumSize(new Dimension(800,300));
		createComponents();
		init();
		pack();
		setVisible(true);
	}  
	
	private void createComponents(){
		databaseURL = new JTextField("jdbc:postgresql://db.ist.utl.pt:5432/ist422670");
		databaseUser = new JTextField();
	    databasePass = new JPasswordField();
	    databaseConnect = new JButton("Connect");
	    databaseConnect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startService();
			}
		});
	}
	
	/**
	 * Metoda inicjujaca okno aplikacji serwera, wolana z konstruktora klasy FBServerView.
	 */
	private void init(){
		GridLayout gridLayout = new GridLayout(1,2);
		gridLayout.setHgap(100);
		gridLayout.setVgap(20);
		JPanel logging = new JPanel(gridLayout);
		logging.setBorder(BorderFactory.createTitledBorder("Database"));
		logArea =new TextArea();
		logArea.setEditable(false);
		setLayout(new BorderLayout());
		add(logging, BorderLayout.NORTH);
		add(logArea, BorderLayout.CENTER);
		
        //Panel logowania
        JPanel infoPanel = new JPanel(new BorderLayout());
        JPanel commandInfoPanel = new JPanel(new GridLayout(3,1)); 
        JPanel textInfoPanel = new JPanel(new GridLayout(3,1)); 
        commandInfoPanel.add(new JLabel("    Address:    "));
        commandInfoPanel.add(new JLabel("    Username:    "));
        commandInfoPanel.add(new JLabel("    Password:    "));
        textInfoPanel.add(databaseURL);
        textInfoPanel.add(databaseUser);
        textInfoPanel.add(databasePass);
        infoPanel.add(commandInfoPanel, BorderLayout.WEST);
        infoPanel.add(textInfoPanel, BorderLayout.CENTER);
        JPanel infoButtonPanel = new JPanel(new GridLayout(2,1));
        infoButtonPanel.add(databaseConnect);         
        logging.add(infoPanel);
        logging.add(infoButtonPanel);
        
		addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
            System.exit(0);
            }
        }); 
	} 
	
	/**
	 * Metoda ustawiajaca mozliwosc edytowania pol identyfikujacych serwer i uzytkownika bazy danych.
	 */
	void updateDatabasePanel(boolean choice){
		databaseURL.setEnabled(choice);
		databaseUser.setEnabled(choice);
		databasePass.setEnabled(choice);
		databaseConnect.setEnabled(choice);
	}
	
	void  startService(){
		if(databaseURL.equals("") || databaseUser.equals("") || databasePass.equals("")){
			printInfo("Please, fill in all the fields.");
		}
		else{
			try {
				server.startService(databaseURL.getText(), databaseUser.getText(), databasePass.getText());
				updateDatabasePanel(false);
			} catch (RemoteException e) {
				printInfo("Error in starting server: " + e);
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Metoda prezentujaca na konsoli informacje.
	 */
	void printInfo(String info){
		logArea.append(info+"\n");
	}
	
}
