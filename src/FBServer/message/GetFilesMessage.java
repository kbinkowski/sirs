package FBServer.message;

import java.io.Serializable;

public class GetFilesMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private String login;
	private long timestamp;
	
	public GetFilesMessage(String login, long timestamp) {
		this.setLogin(login);
		this.setTimestamp(timestamp);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
}
