package FBServer.message;

import java.io.Serializable;

public class SendFileMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String login;
	private String path;
	private long timestamp;
	
	public SendFileMessage(String login, String path, long timestamp) {
		this.setLogin(login);
		this.setPath(path);
		this.setTimestamp(timestamp);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
}
