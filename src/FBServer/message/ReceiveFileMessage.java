package FBServer.message;

import java.io.Serializable;

public class ReceiveFileMessage implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String login;
	private String path;
	private String checksum;
	private long size;
	private long timestamp;
	
	public ReceiveFileMessage(String login, String path, String checksum, long size, long timestamp) {
		this.setLogin(login);
		this.setPath(path);
		this.setChecksum(checksum);
		this.setSize(size);
		this.setTimestamp(timestamp);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
}
