package FBServer.message;

import java.io.Serializable;

public class CheckFileMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private String login;
	private String path;
	private String checksum;
	private boolean checkSize;
	private long timestamp;
	
	public CheckFileMessage(String login, String path, String checksum, boolean checkSize, long timestamp) {
		this.setLogin(login);
		this.setPath(path);
		this.setChecksum(checksum);
		this.setCheckSize(checkSize);
		this.setTimestamp(timestamp);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public boolean isCheckSize() {
		return checkSize;
	}

	public void setCheckSize(boolean checkSize) {
		this.checkSize = checkSize;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
}
