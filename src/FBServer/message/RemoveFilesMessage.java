package FBServer.message;

import java.io.Serializable;
import java.util.ArrayList;

public class RemoveFilesMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private String login;
	private ArrayList<String> paths;
	private long timestamp;
	
	public RemoveFilesMessage(String login, ArrayList<String> paths, long timestamp) {
		this.setLogin(login);
		this.setPaths(paths);
		this.setTimestamp(timestamp);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public ArrayList<String> getPaths() {
		return paths;
	}

	public void setPaths(ArrayList<String> paths) {
		this.paths = paths;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
}
