package FBServer;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.AccessException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;

import javax.crypto.Cipher;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;

import FBClient.FBClientInterface;
import FBServer.database.DatabaseUtils;
import FBServer.message.CheckFileMessage;
import FBServer.message.GetFilesMessage;
import FBServer.message.ReceiveFileMessage;
import FBServer.message.RemoveFilesMessage;
import FBServer.message.SendFileMessage;

import com.healthmarketscience.rmiio.RemoteInputStream;
import com.healthmarketscience.rmiio.RemoteInputStreamClient;
import com.healthmarketscience.rmiio.SimpleRemoteInputStream;

public class FBServer extends UnicastRemoteObject implements FBServerInterface {
	private static final long serialVersionUID = 1L;

	/**
	 * Pole reprezentujace nazwe serwera po ktorej jest identyfikowany
	 * (adres+port+usluga) - odczytywana z pliku konfiguracyjnego.
	 */
	String nameServer;

	/**
	 * Pole reprezentujace port na ktorym uruchomiony jest serwer. Zawarty w
	 * nazwie serwera.
	 */
	int portServer;

	/**
	 * Pole reprezentujace widok serwera.
	 */
	FBServerView serverView;

	/**
	 * Klienci znajdujacy sie w bazie serwera identyfikowani po nazwie i hasle.
	 */
	HashMap<String, String> clients;

	/**
	 * Aktualnie zalogowani klienci identyfikowani po unikatowej nazwie,
	 * reprezentowani przez service jaki ich obsluguje.
	 */

	HashSet<String> actualClients;	
	

	FBServerSecurity fbServerSecurity;

	/**
	 * Konstruktor klienta usugi archiwizatora.
	 */
	FBServer() throws RemoteException {

		clients = new HashMap<String, String>();
		actualClients = new HashSet<String>();
		fbServerSecurity = new FBServerSecurity();
		
		try {
			
			FileInputStream inputStream = new FileInputStream("config.properties");
			Properties properties = new Properties();
			properties.load(inputStream);
			
			portServer = Integer.parseInt(properties.getProperty("port"));
			nameServer = "rmi://" + properties.getProperty("ip") + ":" + portServer + "/backuper";
			
			inputStream.close();
			
		} catch (Exception e) {
			System.err.println("Error in reading properties: " + e);
			System.exit(1);
		}
		
		final FBServer me = this;
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					serverView = new FBServerView(me);
				} catch (Exception e) {
					System.err.println("Error in creating ServerView: " + e);
					System.exit(1);
				}
			}
		});
	}
	
	
	/**
	 * Uruchomienie funkcji serwera.
	 */	
	void  startService(String url_, String user_, String password_) throws AccessException, RemoteException{	
		DatabaseUtils.setParameters(url_, user_, password_);
		LocateRegistry.createRegistry(getPort()).rebind("backuper", this);
		printInfo("Server with a name: " + getName() + " has been activated");
	}

	/**
	 * Metoda wylogowania klienta z serwera
	 */
	public boolean logOff(String login, byte[] msg) throws RemoteException {
		byte[] decipheredMsg = fbServerSecurity.cipherMessage(msg, login, Cipher.DECRYPT_MODE);
		Long timestamp = Long.parseLong(new String(decipheredMsg));
		
		if(!fbServerSecurity.checkTimestamp(login, timestamp))
			return false;
		
		synchronized (actualClients) {
			actualClients.remove(login);
		}
		printInfo("User: " + login + " has been logged out.");
		return true;
	}

	/**
	 * Metoda pobierania pliku z klienta do serwera
	 */
	public int receiveFileFromClient(byte[] msg, RemoteInputStream remoteInputStream, FBClientInterface client) throws IOException {
		byte[] decipheredMsg = fbServerSecurity.cipherMessage(msg, client.getName(), Cipher.DECRYPT_MODE);
		ReceiveFileMessage message = SerializationUtils.deserialize(decipheredMsg);	
		if(!fbServerSecurity.checkTimestamp(message.getLogin(), message.getTimestamp()))
			return -1;
		
		Connection connection = null;
		InputStream inputStream = null;
		InputStream inputStreamEncrypted = null;
		int result = 0;

		try {
			connection = DatabaseUtils.getConnection();
			inputStreamEncrypted = RemoteInputStreamClient.wrap(remoteInputStream);
			byte[] cipherFile = fbServerSecurity.cipherMessage(IOUtils.toByteArray(inputStreamEncrypted), message.getLogin(), Cipher.DECRYPT_MODE);
			inputStream = new ByteArrayInputStream(cipherFile);

			client.updateProgressBar(33);

			PreparedStatement ps = connection.prepareStatement("INSERT INTO file_info (login, path, checksum, size) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, message.getLogin());
			ps.setString(2, message.getPath());
			ps.setString(3, message.getChecksum());
			ps.setLong(4, message.getSize());

			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			Timestamp datetime = rs.getTimestamp("datetime");

			rs.close();
			ps.close();

			client.updateProgressBar(66);

			ps = connection.prepareStatement("INSERT INTO file_content VALUES (?, ?, ?, ?)");
			ps.setString(1, message.getLogin());
			ps.setString(2, message.getPath());
			ps.setTimestamp(3, datetime);
			ps.setBinaryStream(4, inputStream, message.getSize());
			ps.executeUpdate();
			
			ps.close();
			client.updateProgressBar(99);
			printInfo("Finished writing file " + message.getPath());
			
		} catch (Exception e) {
			printInfo("Error in transfering file! File hasn't been written on server.");
			printInfo(e.getMessage());
			result = -1;
		} finally {
			try {
				inputStream.close();
				inputStreamEncrypted.close();
				connection.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				client.updateProgressBar(100);
			}
		}

		return result;
	}

	/**
	 * Metoda sprawdzania pliku. 
	 */
	public long checkFileOnServer(byte[] msg, FBClientInterface client) throws RemoteException {
		byte[] decipheredMsg = fbServerSecurity.cipherMessage(msg, client.getName(), Cipher.DECRYPT_MODE);
		CheckFileMessage message = SerializationUtils.deserialize(decipheredMsg);
		if(!fbServerSecurity.checkTimestamp(message.getLogin(), message.getTimestamp()))
			return -3;
		
		Connection connection = null;
		long result = -1; // Not newest version of file on server, newest version needs to be copied

		try {
			connection = DatabaseUtils.getConnection();

			PreparedStatement ps = null;
			if (message.getPath().contains("|")) {
				ps = connection.prepareStatement("SELECT checksum, size FROM file_info WHERE login = ? AND path = ? AND datetime = ?");
				ps.setString(2, message.getPath().split("\\|")[0]);
				ps.setTimestamp(3, new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(message.getPath().split("\\|")[1]).getTime()));
			} else {
				ps = connection.prepareStatement("SELECT checksum, size FROM file_info WHERE login = ? AND path = ? ORDER BY datetime DESC LIMIT 1");
				ps.setString(2, message.getPath());
			}
			ps.setString(1, message.getLogin());

			ResultSet rs = ps.executeQuery();
			if (!rs.next()) {
				result = 0;
			} else if (message.isCheckSize()) {
				result = rs.getLong("size");
			} else if (message.getChecksum().equals(rs.getString("checksum"))) {
				result = -2; // newest file on server, no need of copying
			}

			rs.close();
			ps.close();
		} catch (Exception e) {
			printInfo("Error in checking a file on server");
			printInfo(e.getMessage());
			result = -3;
		} finally {
			try {
				connection.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * Pobieranie listy archiwizowanych plikow z serwera do klienta
	 */
	public byte[] getFileList(byte[] msg, FBClientInterface client) throws RemoteException {
		byte[] decipheredMsg = fbServerSecurity.cipherMessage(msg, client.getName(), Cipher.DECRYPT_MODE);
		GetFilesMessage message = SerializationUtils.deserialize(decipheredMsg);
		if(!fbServerSecurity.checkTimestamp(message.getLogin(), message.getTimestamp()))
			return null;
		
		HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
		Connection connection = null;
		
		try {
			connection = DatabaseUtils.getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT path, datetime FROM file_info WHERE login = ?");
			ps.setString(1, message.getLogin());

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String path = rs.getString("path");
				String datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(rs.getTimestamp("datetime").getTime()));

				if (!result.containsKey(path))
					result.put(path, new ArrayList<String>());
				result.get(path).add(datetime);
			}

			rs.close();
			ps.close();
		} catch (Exception e) {
			printInfo("Error in getting files from server");
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return fbServerSecurity.cipherMessage(SerializationUtils.serialize(result), client.getName(), Cipher.ENCRYPT_MODE);
	}

	/**
	 * Wysylanie pliku do klienta
	 */
	public RemoteInputStream sendFileToClient(byte[] msg, FBClientInterface client) throws RemoteException {
		byte[] decipheredMsg = fbServerSecurity.cipherMessage(msg, client.getName(), Cipher.DECRYPT_MODE);
		SendFileMessage message = SerializationUtils.deserialize(decipheredMsg);
		if(!fbServerSecurity.checkTimestamp(message.getLogin(), message.getTimestamp()))
			return null;
		
		Connection connection = null;
		SimpleRemoteInputStream inputStream = null;

		try {
			connection = DatabaseUtils.getConnection();

			PreparedStatement ps = null;
			if (message.getPath().contains("|")) {
				ps = connection.prepareStatement("SELECT file FROM file_content WHERE login = ? AND path = ? AND datetime = ?");
				ps.setString(2, message.getPath().split("\\|")[0]);
				ps.setTimestamp(3, new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(message.getPath().split("\\|")[1]).getTime()));
			} else {
				ps = connection.prepareStatement("SELECT file FROM file_content WHERE login = ? AND path = ? ORDER BY datetime DESC LIMIT 1");
				ps.setString(2, message.getPath());
			}
			ps.setString(1, message.getLogin());

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				byte[] encryptedFile = fbServerSecurity.cipherMessage(rs.getBytes("file"), message.getLogin(), Cipher.ENCRYPT_MODE);
				inputStream = new SimpleRemoteInputStream(new ByteArrayInputStream(encryptedFile));
			}

			rs.close();
			ps.close();
		} catch (Exception e) {
			printInfo("Error in sending a file to client");
			printInfo(e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		if (inputStream != null){
			printInfo("Finished exporting file " + message.getPath());
			return inputStream.export();
		}
		return null;
	}

	/**
	 * Usuwanie plikow z serwera
	 */
	public int removeFilesFromServer(byte[] msg, FBClientInterface client) throws RemoteException {
		byte[] decipheredMsg = fbServerSecurity.cipherMessage(msg, client.getName(), Cipher.DECRYPT_MODE);
		RemoveFilesMessage message = SerializationUtils.deserialize(decipheredMsg);
		if(!fbServerSecurity.checkTimestamp(message.getLogin(), message.getTimestamp()))
			return -1;
		
		Connection connection = null;
		try {
			connection = DatabaseUtils.getConnection();

			PreparedStatement ps = null;
			for (String path : message.getPaths()) {
				if (path.contains("|")) {
					ps = connection.prepareStatement("DELETE FROM file_info WHERE login = ? AND path = ? AND datetime = ?");
					ps.setString(2, path.split("\\|")[0]);
					ps.setTimestamp(3, new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(path.split("\\|")[1]).getTime()));
				} else {
					ps = connection.prepareStatement("DELETE FROM file_info WHERE login = ? AND path = ?");
					ps.setString(2, path);
				}
				ps.setString(1, message.getLogin());
				ps.executeUpdate();
				printInfo("Finished deleting file " + path);
			}

			ps.close();
		} catch (Exception e) {
			printInfo("Error in removing files from server");
			printInfo(e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return 0;
	}

	/**
	 * Metoda zwracajaca nazwe serwera.
	 */
	String getName() {
		return nameServer;
	}

	/**
	 * Metoda zwracajaca port na ktorym uruchomiony jest serwer.
	 */
	int getPort() {
		return portServer;
	}

	/**
	 * Metoda prezentujaca na konsoli informacje.
	 */
	void printInfo(String info) {
		serverView.printInfo(info);
	}

	public boolean authInit(String login, byte[] message, FBClientInterface client) {
		try {
			DatabaseUtils.getPassword(login).getBytes();
			if(actualClients.contains(login))
				return false;
			byte[] hashPassword = DatabaseUtils.getPassword(login).getBytes("Windows-1250");
			byte[] encryptedSessionKey = fbServerSecurity.generateKeySession(
					hashPassword, message, login);
			return client.exchangeSessionKey(encryptedSessionKey);
		} catch (Exception e) {		
			e.printStackTrace();
			return false;
		}
	
	}

	public boolean challenge(String login, byte[] encryptedChallenge,
			FBClientInterface client) throws RemoteException {
		try {
			byte[] encryptedChalange = fbServerSecurity.respond2Challenge(
					login, encryptedChallenge);
			
			return client.challenge(encryptedChalange);
		} catch (Exception e) {	
			e.printStackTrace();
			return false;
		}
	}

	public boolean authFinish(String login, byte[] encryptedChallenge)
			throws RemoteException {
		try {
			if (fbServerSecurity.verifyChallenges(login, encryptedChallenge)) {
				actualClients.add(login);
				printInfo("User: " + login + " has been logged in.");
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;		
		}
	}	
	
	/**
	 * Glowna metoda klasy.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			System.setSecurityManager(new RMISecurityManager());
		} catch (SecurityException e) {
			System.err.println("Security violation " + e);
			System.exit(1);
		}

		try {
			new FBServer();
		} catch (Exception e) {
			System.err.println("Error in creating Server: " + e);
			System.exit(1);
		}
	}

}
