package FBServer.database;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import SecurityUtils.SecurityUtils;

public class DatabaseUtils {
	
	private static String url;
	private static String user;
	private static String password;
	
	public static void setParameters(String url_, String user_, String password_){
		url = url_; 
		user = user_;
		password = password_;
	}
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("org.postgresql.Driver");
		Connection connection = DriverManager.getConnection(url, user, password);
		return connection;
	}
	
	public static String getPassword(String login) throws ClassNotFoundException, SQLException {
		String password = null;
		
		Connection connection = getConnection();
		PreparedStatement ps = connection.prepareStatement("SELECT password FROM account WHERE login = ?");
		ps.setString(1, login);	
		
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			password = rs.getString("password");	
		}
			
		rs.close();
		ps.close();
		
		return password;
	}
	
	public static void addUser(String login, String password) throws ClassNotFoundException, SQLException, NoSuchAlgorithmException {
		Connection connection = getConnection();
		
		PreparedStatement ps = connection.prepareStatement("INSERT INTO account values(?,?,?)");
		ps.setString(1, login);	
		ps.setString(2, new String(SecurityUtils.hashArray("SHA-256", password.getBytes())));	
		ps.setInt(3, 0);
		
		ps.executeUpdate();			
		ps.close();
	}
	
}
	