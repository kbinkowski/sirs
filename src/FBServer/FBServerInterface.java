package FBServer;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import com.healthmarketscience.rmiio.RemoteInputStream;
import FBClient.FBClientInterface;

/**
 * Klasa zawierajaca metody udostepniane klientowi przy pomocy rmi
 * @author F-B team
 */
public interface FBServerInterface extends Remote {
	boolean logOff(String login, byte[] msg) throws RemoteException;
	
	int receiveFileFromClient(byte[] msg, RemoteInputStream remoteInputStream, FBClientInterface client) throws IOException;
	RemoteInputStream sendFileToClient(byte[] msg, FBClientInterface client) throws RemoteException;
	long checkFileOnServer(byte[] msg, FBClientInterface client) throws RemoteException;
	byte[] getFileList(byte[] msg, FBClientInterface client) throws RemoteException;
	int removeFilesFromServer(byte[] msg, FBClientInterface client) throws RemoteException;
	
	boolean authInit(String login, byte[] message, FBClientInterface client) throws RemoteException;
	boolean challenge(String login, byte[] encryptedChallenge, FBClientInterface client)  throws RemoteException;
	boolean authFinish(String login, byte[] encryptedChallenge) throws RemoteException;
}
